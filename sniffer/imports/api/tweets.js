import { Mongo } from 'meteor/mongo';

export const Tweets = new Mongo.Collection('tweets');

Tweets.allow({
    insert() { return true }
});

Tweets.deny({
    insert() { return false }
});

//filter types: 1 - keyword, 2 - userId, 3 - location
const TweetSchema = new SimpleSchema({
    tweet_id: { type: Number, optional: true },
    is_retweeted: { type: Boolean, defaultValue: false },
    is_reply: { type: Boolean, defaultValue: false },
    timestamp: { type: String },
    filter_type: { type: Number, defaultValue: 1 },
    createdAt: { type: Date },
    tweet: { type: String },
    user_id: { type: Number },
    user_profile_image_url: { type: String },
    user_name: { type: String }
});

Tweets.attachSchema(TweetSchema);

