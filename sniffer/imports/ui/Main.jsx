import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Toggle from 'material-ui/Toggle';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import App from './App';

export default class Main extends Component {

    constructor(props) {
        super(props);
        this.state = {
            is_retweeted: false,
            is_reply: false,
            filter_type: 0
        }
    }

    setFilters(field, value) {
        const self = this;
        const current_state = self.state;
        current_state[field] = value;
        self.setState(current_state);
    }

    onFilterTypeChange(event, index, value) {
        this.setState({
            filter_type: value
        })
    }

    render() {

        const self = this;

        return(
            <MuiThemeProvider>
                <div>
                    <div className="container">
                        <div style={ { padding: '30px 0 30px 0' } }>
                            <div className="row">
                                <div className="col s3">
                                    <Toggle
                                        label="Show retweeted tweets"
                                        defaultToggled={false}
                                        onToggle={ (event, isInputChecked) => { this.setFilters('is_retweeted', isInputChecked) } }
                                    />
                                </div>
                                <div className="col s3">
                                    <Toggle
                                        label="Show reply tweets"
                                        defaultToggled={false}
                                        onToggle={ (event, isInputChecked) => { this.setFilters('is_reply', isInputChecked) } }
                                    />
                                </div>
                            </div>
                            <div className="row">
                                <div className="col s3">
                                    <div className="col s6">
                                        <SelectField
                                            floatingLabelText="Filtered by"
                                            value={this.state.filter_type}
                                            onChange={ this.onFilterTypeChange.bind(this) } >
                                            <MenuItem value={0} primaryText="All" />
                                            <MenuItem value={1} primaryText="Keyword" />
                                            <MenuItem value={2} primaryText="userIDs" />
                                            <MenuItem value={3} primaryText="Location" />
                                        </SelectField>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <App is_retweeted={ self.state.is_retweeted }
                         is_reply={ self.state.is_reply }
                         filter_type={ self.state.filter_type } />
                </div>
            </MuiThemeProvider>
        )
    }

}