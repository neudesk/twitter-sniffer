import { Meteor } from 'meteor/meteor';
import React, { Component, PropTypes } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { createContainer } from 'meteor/react-meteor-data';
import { Tweets } from '../api/tweets';
import Autocomplete from 'react-google-autocomplete';
import NotificationSystem from 'react-notification-system';

export class AppComponent extends Component {

    _notificationSystem = null;

    constructor(props) {
        super(props)
        this.state = {
            location: [],
            follow: null,
            track: null,
            is_streaming: false,
            tweets: []
        }
    }

    componentDidMount() {
        this._notificationSystem = this.refs.notificationSystem;
        window.FILTERS = this.state.filters
    }

    initStream() {
        const self = this;
        const params = {}
        if (self.state.location.length) {
            params['location'] = self.state.location
        }
        if (self.state.follow) {
            params['follow'] = self.state.follow
        }
        if (self.state.track) {
            params['track'] = self.state.track
        }
        Meteor.call('initStream', params, (error) => {
            if (error) {
                console.log('error', error)
            } else {
                console.log('stream has been created.')
                self.setState({
                    is_streaming: true
                }, () => {
                    console.log('state', self.state)
                    this._notificationSystem.addNotification({
                        message: 'Listening now to Twitter stream',
                        level: 'success'
                    });
                });
            }
        });
    }

    stopStream() {
        const self = this;
        if (self.state.is_streaming) {
            Meteor.call('stopStream');
            self.setState({
                is_streaming: null
            }, () => {
                this._notificationSystem.addNotification({
                    message: 'Stream listener is now halted.',
                    level: 'warning'
                });
            })
        } else {
            this._notificationSystem.addNotification({
                message: 'No running stream listener',
                level: 'warning'
            });
        }
    }

    onChange(field, e) {
        const self = this;
        const current_state = self.state;
        current_state[field] = e.target.value;
        self.setState(current_state);
    }

    setLocationCoord(place) {
        console.log('place', place)
        const geo = place.geometry.viewport
        this.setState({
            location: [geo.b.b.toString(), geo.f.f.toString(), geo.b.f.toString(), geo.f.b.toString()]
        })
    }

    componentWillReceiveProps(nextProps) {
        const tweets = nextProps.tweets;
        this.setState({
            tweets: tweets
        })
    }

    render() {

        let tweetCards = null;

        tweetCards = this.state.tweets.map((tweet, key) => {
            return (
                <div key={ key } className="col s12">
                    <div className="card horizontal" style={ { padding: '20px' } }>
                        <div className="card-image">
                            <img src={ tweet.user_profile_image_url } />
                        </div>
                        <div className="card-stacked" style={ { paddingLeft: '20px' } }>
                            <div className="card-content" style={ {padding: 0} }>
                                <span className={ 'new badge darken-1 ' + (tweet.is_retweeted ? 'blue' : 'green') } data-badge-caption={ tweet.is_retweeted ? 're-tweeted' : 'new' }></span>
                                <h5 style={ { margin: 0 } }>{ tweet.user_name }</h5>
                                <small>{tweet.user_id}</small>
                                <p>{ tweet.tweet }</p>
                            </div>
                        </div>
                    </div>
                </div>
            )
        });

        return(
            <MuiThemeProvider>
                <div className="container">
                    <div className="row">
                        <div className="col s5">
                            <div style={ { marginTop: '40%' } }>
                                <div className="row">
                                    <div className="input-field col s6">
                                        <input id="follow" ref="follow" type="text" className="validate"
                                               onChange={ (e) => { this.onChange('follow', e) } }
                                               placeholder="i.e some, twitter, handles"
                                               disabled={ this.state.is_streaming } />
                                        <label className="active" htmlFor="follow">Follow</label>
                                    </div>
                                    <div className="input-field col s6">
                                        <Autocomplete
                                            onPlaceSelected={(place) => {
                                                this.setLocationCoord(place)
                                            }}
                                            types={['(cities)']}
                                            disabled={ this.state.is_streaming }
                                        />
                                        <label className="active" htmlFor="location">Location</label>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="input-field col s12">
                                        <input id="keyword" ref="track" type="text"
                                               className="validate" onChange={ (e) => { this.onChange('track', e) } }
                                               placeholder="i.e. some, keywords"
                                               disabled={ this.state.is_streaming } />
                                        <label className="active" htmlFor="keyword">Keyword</label>
                                    </div>
                                </div>
                                <div className="row">
                                    <a className="waves-effect waves-light btn blue" onClick={ () => { this.state.is_streaming ? this.stopStream() : this.initStream() } }>
                                        <i className="material-icons left">cloud</i> { this.state.is_streaming ? 'Halt the stream' : 'Let the tweet flows' }
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div className="col s7">
                            {tweetCards}
                        </div>
                    </div>
                    <NotificationSystem ref="notificationSystem" />
                </div>
            </MuiThemeProvider>
        )
    }

}

AppComponent.propTypes = {
    tweets: PropTypes.array.isRequired,
}

export default App = createContainer((props) => {
    Meteor.subscribe('tweets');
    const params = {is_retweeted: props.is_retweeted, is_reply: props.is_reply}
    if (props.filter_type != 0) {
        params.filter_type = props.filter_type
    }
    console.log('params', params)
    return {
        tweets: Tweets.find({ ...params }, {limit: 5, sort: { createdAt: -1 } }).fetch()
    };
}, AppComponent);
