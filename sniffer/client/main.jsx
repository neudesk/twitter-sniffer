import { Meteor } from 'meteor/meteor';
import React from 'react';
import { render } from 'react-dom';
import injectTapEventPlugin from 'react-tap-event-plugin';

import Main from '../imports/ui/Main';

injectTapEventPlugin();

Meteor.startup(() => {
    render(<Main />, document.getElementById('yield'));
});