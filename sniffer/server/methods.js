import { Meteor } from 'meteor/meteor';
import Twit from 'twit';
import * as _ from 'lodash';

import { Tweets } from '../imports/api/tweets';

const Twitter = new Twit({
    consumer_key: 'jYJrm0ENfqavOSoGjPXDZvdJS',
    consumer_secret: 'YAkVCWOw4LJ3N1LJmtEyMB7ARcL3M0Tt3g8PAOteLGsTlyr9Cr',
    access_token: '886046613860261889-7j3aXU2UqCYpSt9z1Y592mym8iZQnhx',
    access_token_secret: 'GHs5IHodTbiK65tO1isEVeCAox1utTeiw81tcJkHJbq4x',
    timeout_ms: 60 * 1000
});

var stream = null;

const isTweetReply = (res) => {
    let result = false;
    const fields = ['in_reply_to_status_id', 'in_reply_to_status_id_str', 'in_reply_to_user_id', 'in_reply_to_user_id_str', 'in_reply_to_screen_name']
    fields.map((field) => {
        if (res[field]) {
            result = true
        }
    });
    return result;
}

const getFilterType = (params, res) => {
    let result = 1;
    let userIds = [];
    if (params.follow) {
        userIds = params.follow.split(',');
    }
    if (userIds.length) {
        if (_.contains(userIds, res.in_reply_to_user_id_str) || _.contains(userIds, res.in_reply_to_user_id) || _.contains(userIds, res.user.id)) {
            result = 2;
        }
        if (res.retweeted_status) {
            if (_.contains(userIds, res.retweeted_status.in_reply_to_user_id_str) || _.contains(userIds, res.retweeted_status.in_reply_to_user_id) || _.contains(userIds, res.retweeted_status.user.id)) {
                result = 2;
            }
        }
    }
    if (res.geo) {
        result = 3;
    }
    return result;
}

Meteor.methods({
    initStream(params) {
        console.log('params', params)
        stream = Twitter.stream('statuses/filter', params);

        const wrappedInsert = Meteor.bindEnvironment(function(res) {
            // console.log('res', res);
            Tweets.insert({
                tweet_id: res.id,
                is_retweeted: res.retweeted,
                is_reply: isTweetReply(res) ? true : false,
                timestamp: res.timestamp_ms,
                filter_type: getFilterType(params, res),
                createdAt: new Date(),
                tweet: res.truncated ? res.extended_tweet.full_text : res.text,
                user_id: res.user.id,
                user_profile_image_url: res.user.profile_image_url,
                user_name: res.user.name
            });
        });

        stream.on('tweet', (res) => {
            wrappedInsert(res);
        });

        stream.on('limit', function (limitMessage) {
            console.log('RESTRICTED', limitMessage);
        })

        stream.on('scrub_geo', function (scrubGeoMessage) {
            console.log('scrub_geo', scrubGeoMessage);
        })

        stream.on('disconnect', function (disconnectMessage) {
            console.log('DISCONNECTED', disconnectMessage);
        })

        stream.on('connect', function (request) {
            console.log('CONNECTING');
        })

        stream.on('connected', function (response) {
            console.log('CONNECTED');
        })

        stream.on('reconnect', function (request, response, connectInterval) {
            console.log('RECONNECTING', 'reconnecting in : ' + connectInterval);
        })

    },

    stopStream() {
        if (stream) {
            stream.stop()
        }
    }

})