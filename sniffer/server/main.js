import { Meteor } from 'meteor/meteor';
import { Tweets } from '../imports/api/tweets';

Meteor.startup(() => {
    Meteor.publish('tweets', function() {
        return Tweets.find({});
    })
});
