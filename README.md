# Crowdynews Node.js assignment
Build a Node.js application that retrieves content from twitter's [public stream API](https://dev.twitter.com/streaming/reference/post/statuses/filter),
processes it and stores it.

## Application Installation
 - Install nodeJS (https://nodejs.org/en/) 
 - Install MeteorJS (https://www.meteor.com/install)
 - Clone the repository then change directory to the app root.
```shell
 git clone https://neudesk@bitbucket.org/neudesk/twitter-sniffer.git
 cd twitter-sniffer/sniffer
```
 - Install application dependencies
```shell
 meteor npm install
```
 - Run the app
```shell
 meteor
```
 
## Application Requirements
 - The application must accept the 3 supported twitter-filters; `phrases/keywords`, `userIds`, and `locations`.
 - Create a content-filter that filters out all retweets and replies from the incoming twitter stream.
 - Store the retrieved content in MongoDB.
 - It should be able to handle multiple types of twitter-filters in a single connection e.g:

```json
{
	"follow": "twitterapi",
	"track": "the twitter,more phrases,foo,bar",
	"locations": "-74,40,-73,41"
}
```

 - Must be performant and able to run without crashing with high volume twitter-filters (e.g. `keyword "trump"`).
 - Devise a way to detect for which twitter-filter(s) the content items result from.
 - You must be able to query the stored content items based on the detected twitter-filter, _efficiently_.
 - Implement error handling, including rate limits.

## Design
 - A way to handle twitter's backoff strategy in your application.
 - A way to handle bottlenecks in your application.

 *_Note that you're not required to implement the designs mentioned above._

 **_Note that you're encouraged to document any issues that you faced and/or your design decisions._

